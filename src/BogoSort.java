import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class BogoSort {
	private boolean isSorted(int[] arr)
	{
		int ctr = 0;
		while(ctr<arr.length-2)
		{
			if(arr[ctr]<=arr[ctr+1]);
			else return false;
			ctr++;
		}
		return true;
	}
	
	private void swap(int index1, int index2, int[] array)
	{
		int a,b;
		a = array[index1];
		b = array[index2];
		
		array[index1] = b;
		array[index2] = a;
		
//		System.out.println("swap " + a + " with " + b);
	}
	
	long Bogosort(int[] arr)
	{
		Random random = new Random();
		long loopcount = 0;
		while(!isSorted(arr))
		{
			swap(random.nextInt(arr.length-1),random.nextInt(arr.length-1),arr);
			loopcount++;
		}
		return loopcount;
	}
}
