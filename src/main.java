import java.util.Random;

public class main {
	public static void main(String[] args)
	{
		int[] array = new int[14];
		
		BogoSort sortie = new BogoSort();
		
		fillWithRandom(array);
		
		System.out.println("Finished in: " + sortie.Bogosort(array) + " Cycles");
	}
	
	public static void fillWithRandom(int[] array)
	{
		int size = array.length;
		Random random = new Random();
		
		while(size>0)
		{
			array[size-1] = random.nextInt(100);
			size--;
		}
	}
}
